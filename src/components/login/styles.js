
const React = require('react-native');

const { StyleSheet, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;

export default {
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#0058c1',
  },
  shadow: {
    flex: 1,
    width: null,
    height: null,
  },
  bg: {
    flex: 1,
    marginTop: deviceHeight / 4.50,
    paddingTop: 20,
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 30,
    bottom: 0,
  },
  input: {
    marginBottom: 35,
  },
  btn: {
    marginTop: 20,
    alignSelf: 'center',
  },
  text: {
    marginTop: 15,
    alignSelf: 'center',
    fontSize: 10,
    color: 'white',
  },
  textBtn: {
    fontSize: 18,
    color: '#0058c1',
  },
};
