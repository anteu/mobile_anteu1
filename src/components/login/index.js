import React, { Component } from 'react';
import { Image, StatusBar, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import {
  Container,
  Content,
  Item,
  Input,
  Button,
  Icon,
  View,
  Text,
} from 'native-base';

import { Field, reduxForm } from 'redux-form';
import { setUser } from '../../actions/user';
import styles from './styles';
import myTheme from '../../themes/base-theme'

const background = require('../../../images/shadow.png');

const validate = values => {
  const error = {};
  error.email = '';
  error.password = '';
  var ema = values.email;
  var pw = values.password;
  if (values.email === undefined) {
    ema = '';
  }
  if (values.password === undefined) {
    pw = '';
  }
  if (ema.length < 8 && ema !== '') {
    error.email = 'curto demais';
  }
  if (!ema.includes('@') && ema !== '') {
    error.email = '@ não incluso';
  }
  if (pw.length > 12) {
    error.password = 'no máximo 11 caracteres';
  }
  if (pw.length < 5 && pw.length > 0) {
    error.password = 'Fraca';
  }
  return error;
};

class Login extends Component {
  static propTypes = {
    setUser: React.PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      name: '',
    };
    this.renderInput = this.renderInput.bind(this);
  }

  setUser(name) {
    this.props.setUser(name);
  }
  renderInput({
    input,
    label,
    type,
    meta: { touched, error, warning },
    inputProps
  }) {
    var hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <Item error={hasError}>
        <Icon active style={{ color: 'white' }} name={input.name === 'email' ? 'person' : 'unlock'} />
        <Input
          placeholder={input.name === 'email' ? 'Email' : 'Senha'}
          {...input}
        />
        {hasError
          ?
            <Item style={{ borderColor: 'transparent' }}>
              <Icon active style={{ color: 'white', marginTop: 5 }} name='bug' />
              <Text style={{ fontSize: 15, color: 'white' }}>{error}</Text>
            </Item>
          : <Text />}
      </Item>
    );
  }
  render() {
    return (
      <Container>
        <StatusBar backgroundColor="#0258c1" barStyle="light-content" />
        <View style={styles.container}>
          <Content theme={myTheme}>
            {/* <Image source={background} /> */}
            {/* <View style={styles.bg}> */}
            <KeyboardAvoidingView behavior='padding'>
              <View style={styles.bg}>
                <Field name='email' component={this.renderInput} />
                <Field name='password' component={this.renderInput} />
                <Button
                  block
                  light
                  // style={styles.btn}
                  onPress={() => this.props.navigation.navigate('Home')}
                >
                  <Text style={styles.textBtn}>ENTRAR</Text>
                </Button>
                <Text style={styles.text}>Esqueceu sua senha?</Text>
              </View>
            </KeyboardAvoidingView>
            <Grid style={styles.btn}>
              <Text style={styles.text}>------------------------------</Text>
              <Text style={styles.text}>  ou  </Text>
              <Text style={styles.text}>------------------------------</Text>
            </Grid>
            <Grid style={styles.btn}>
              <Button
                bordered
                light
                // style={styles.btn}
                onPress={() => this.props.navigation.navigate('Home')}
              >
                <Icon type='EvilIcons' name='sc-google-plus' />
              </Button>

              <Button
                bordered
                light
                // style={styles.btn}
                onPress={() => this.props.navigation.navigate('Home')}
              >
                <Icon type='Ionicons' name='person' />
              </Button>
            </Grid>
            <Text style={styles.text}>Criar uma conta</Text>
          </Content>
        </View>
      </Container>
    );
  }
}
const LoginSwag = reduxForm(
  {
    form: 'test',
    validate
  },
  function bindActions(dispatch) {
    return {
      setUser: name => dispatch(setUser(name))
    };
  }
)(Login);
LoginSwag.navigationOptions = {
  header: null
};
export default LoginSwag;
